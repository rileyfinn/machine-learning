function [principalComponents, eigenValues] = mypca(data, ...
                                                    num_principal_components)
    X = data(:, 1:end - 1);
    % create covariance matrix
    sigmaHat = cov(X);

    % get eigenvalues and corresponding eigenvectors
    [V, D] = eig(sigmaHat);

    % sort eigenvalues in descending order to compute proportion of variance
    % W represents a matrix eigenvectors
    % D represents a diagonal matrix of eigenvalues in descending order
    diagD = diag(D);
    [eigenValuesTotal, eigInd] = sort(diagD, 'descend');
    eigenVectorsTotal = V(:, eigInd);
    eigenValues = eigenValuesTotal(1:num_principal_components);
    principalComponents = eigenVectorsTotal(:, 1: ...
                                        num_principal_components);
end