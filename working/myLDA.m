function [eigenVectors, eigenValues] = mylda(data, ...
                                             num_principal_components)
    train = data;
    x = train(:, 1:end - 1);
    r = train(:, end);
    K = 10;
    N = size(train, 1);
    D = size(x, 2);

    % find sample mean for each class
    m = zeros(K, D);
    for ii = 1:K
        ind = find(r == ii - 1);
        m(ii, :) = mean(x(ind, :));
    end

    % calculate the within-class scatter matrix S_w
    S_i = zeros(64, 64);
    S_w = zeros(64, 64);
    for j = 1:K
        ind = find(r == j - 1);
        xx = x(ind, :);
        kObs = size(xx, 1);
        for k = 1:kObs
            S_i = S_i + (xx(k, :) - m(j, :))' * xx(k, :);
        end
        S_w = S_w + S_i;
    end

    % calculate the inbetween-class scatter matrix
    overallM = mean(m);
    S_b = zeros(64, 64);
    for j = 1:K
        classN(j) = length(find(r == j - 1));
        S_b = classN(j) * (m(j, :) - overallM)' * (m(j, :) - overallM);
    end

    % inv(S_w) is singular so add small values to the diagonal
    adjS_w = S_w + (eye(size(S_w)) / 99999999);
    W = pinv(adjS_w) * S_b;

    % get eigenvalues and eigenvectors
    [V, D] = eig(W);
    % get diagonal values from eigenvalue matrix and store in vector
    diagD = diag(D);

    % sort eig values greatest to least and store index
    [eigenValuesTotal, indD] = sort(diagD, 'descend');

    % get corresponding eigenvectors(which are is w = projection matrix)
    eigenVectorsTotal = V(:, indD);

    eigenValues = eigenValuesTotal(1:num_principal_components);
    eigenVectors = eigenVectorsTotal(:, 1:num_principal_components);
end
