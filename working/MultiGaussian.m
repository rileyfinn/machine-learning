function [pc1, pc2, mu1, mu2, S1, S2, alpha1, alpha2] = ...
        multigaussian(training_data, testing_data, Model)
    train = load(training_data);
    test = load(testing_data);

    trainN = size(train, 1);
    trainD = size(train, 2);
    testN = size(test, 1);
    testD = size(test, 2);
    class1 = train(:, end) == 1;
    class2 = train(:, end) == 2;

    % calculate priors
    pc1 = sum(train(:, end) == 1) / trainN
    pc2 = 1 - pc1;

    % calculate sample means for each dimension
    x1 = train(class1, 1:end - 1);
    x2 = train(class2, 1:end - 1);
    mu1 = mean(x1);
    mu2 = mean(x2);

    if Model == 1
        % model 1 S1 and S2
        S1 = cov(x1);
        S2 = cov(x2);
    end

    if Model == 2
        % model 2 S1 and S2
        S1 = (cov(x1) * pc1) + (cov(x1) * pc2);
        S2 = (cov(x2) * pc1) + (cov(x2) * pc2);
    end

    if Model == 3
        % model 3 alpha1 and alpha2
        alpha1 = zeros(8);
        alpha2 = zeros(8);
        class1N = pc1 * 100;
        class2N = pc2 * 100;
        for ii = 1:class1N
        ii = 1;
        alpha1 = alpha1 + ...
                 (x1(ii, :) - mu1)' * ...
                 (x1(ii, :) - mu1) / ...
                 class1N;
        alpha2 = alpha2 + ...
                 (x2(ii, :) - mu2)' * ...
                 (x2(ii, :) - mu2) / ...
                 class2N;
        end
    end


    % discriminant for model 1 and 2
    g1 = zeros(1, 100);
    g2 = zeros(1, 100);
    testX = test(:, 1:end - 1);
    for ii = 1:100
        g1(ii) = -0.5 * (testX(ii, :) - mu1) * inv(S1) * (testX(ii, :) - mu1)' + log(pc1);
        g2(ii) = -0.5 * (testX(ii, :) - mu2) * inv(S2) * (testX(ii, :) - mu2)' + log(pc2);
    end

    prediction = zeros(1, 100);
    for j = 1:100
        if g1(j) >= g2(j)
            prediction(j) = 1;
        else
            prediction(j) = 2;
        end
    end

    for j = 1:100
        error(j) = prediction(j) ~= test(j, end);
    end

    errorRate = sum(error) / 100
end