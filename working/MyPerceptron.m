% X is the feature matrix, dim(40, 2)
% y is the label vector, dim(40, 1)
% w0 is the initialization of the parameter 'w'
% w is the paramter found by perceptron
% step represents the number of steps the algorithm takes to converge

function [w, step] = myperceptron(X, y, w0)
    n = length(X);
    w = w0;
    xBoundary = -1:0.01:1;
    yNeg = y == -1;
    yPos = y == 1;
    negClass = X(yNeg, :);
    posClass = X(yPos, :);
    posTotal = ones(1, length(negClass));
    negTotal = ones(1, length(negClass));

    for ii = 1:n
        clf
        step = ii;
        x = X(ii, :);
        r = y(ii);

        proposed = dot(w, x') * r';
        if proposed <= 0
            w = w + (r' * x');
        end

        yBoundary = (-(w(1) * xBoundary)) / w(2);
        scatter(negClass(:, 1), negClass(:, 2), 'b', 'filled')
        hold on
        scatter(posClass(:, 1), posClass(:, 2), 'r', 'filled')
        hold on
        plot(xBoundary, yBoundary);
        axis([-1 1 -1 1])

        boundary = [xBoundary(yBoundary <= 1 & yBoundary >= -1)'...
                    yBoundary(yBoundary <= 1 & yBoundary >= -1)'];

        posClass = [posClass(:, 1) posClass(:, 2)];
        negClass = [negClass(:, 1) negClass(:, 2)];

        for j = 1:length(boundary)
            posTotal = boundary(j, 1) >= posClass(:, 1) & ...
                boundary(j, 2) >= posClass(:, 2);
        end

        for k = 1:length(boundary)
            negTotal = boundary(k, 1) <= negClass(:, 1) & ...
                boundary(k, 2) <= posClass(:, 2);
        end

        if sum(posTotal) == 0 & sum(negTotal) == 0
            break
        else
            pause(0.5)
            continue
        end
    end
end
