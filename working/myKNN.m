% training_data = load('optdigits_train.txt');
% test_data = load('optdigits_test.txt');

function [errorRate] = myknn(training_data, test_data, k)
    train = training_data;
    test = test_data;

    testN = size(test, 1);
    prediction = zeros(1, testN);
    for j = 1:testN
        m = size(train, 1);
        n = 1:size(train, 2) - 1;
        pts = zeros(length(m), length(n));
        for ii = 1:m
            pts(ii, n) = (test(j, n) - train(ii, n)) .^ 2;
            eucDis(ii) = sqrt(sum(pts(ii, :)));
        end
        [sorted, sortInd] = sort(eucDis);
        nearest = [sorted(1:k)' sortInd(1:k)'];

        prob = zeros(1, 10);
        for kk = 1:10
            prob(1, kk) = sum((train(nearest(:, 2), end) == kk - 1) ./ k);
        end
        [maxProb, maxInd] = max(prob);
        prediction(j) = maxInd - 1;
    end

    predN = length(prediction);
    error = zeros(1, predN);
    for ii = 1:predN
        error(1, ii) = prediction(ii) ~= test(ii, end);
    end

    formatSpec = 'The error rate for the test set on k = %d is %d';
    er = sum(error) / predN;
    errorRate = sprintf(formatSpec, k, er);
end
