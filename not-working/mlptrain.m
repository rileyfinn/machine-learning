function [z, w, v] = mlptrain(trainingDataFilePath, validationDataFilePath, ...
                              m, k)
    trainingData = load(trainingDataFilePath);
    validationData = load(validationDataFilePath);
    v = zeros(k, m + 1);
    v = -0.01 + 0.02 .* rand(k, m + 1);
    d = size(trainingData(:, 1:end - 1), 2);
    w = zeros(m, d + 1);
    w = -0.01 + 0.02 .* rand(m, d + 1);
    n = size(trainingData, 1);
    y = zeros(n, k);
    z = zeros(n, m);
    r = zeros(n, 1);
    r = trainingData(:, end);
    z = [repmat(1, 1, n)' z]; % add bias vector
    vChange = zeros(k, m + 1);
    wChange = zeros(m, d + 1);
    errorRateOldTrain = 0; % initialize error rates
    errorRateOldValid = 0;

    for iteration = 1:200
        if iteration <= 100
            eta = 0.00001; % trying to improve convergence
        else
            eta = 0.000001;
        end
        
        % loop through rows of training data in random order
        trainingDataRandom = trainingData(randperm(size(trainingData, 1)), :);
        x = [repmat(1, 1, size(trainingData, 1))' trainingData(:, 1:end ...
                                                          - 1)]; 
        r = zeros(n, 1);
        r = trainingDataRandom(:, end);
        for t = 1:n
            for h = 1:m
                o = dot(w(h, :), x(t, :));
                % ReLU activation
                if o < 0 
                    z(t, h) = 0;
                else 
                    z(t, h) = o;
                end
            end
            for ii = 1:k
                o = z(t, :) * v(ii, :)';
                % RELU activation
                if o < 0
                    y(t, ii) = 0;
                else
                    y(t, ii) = o;
                end
                for ii = 1:k
                    vChange(ii, :) = eta * (r(t) - y(t, ii)) * z(t, :);
                end
                for h = 1:m
                    o = dot(w(h, :), x(t, :));
                    if o < 0
                        wChange(m, :) = 0;
                    else
                        wChange(m, :) = eta * (r(t) - y(t, :)) * ...
                            (v(:, h)) * x(t, :);
                    end
                end
            end
            v = v + vChange;
            w = w + wChange;
        end
        % calculate change in error rate
        errorRateTrain(iteration) = (errorRateOldTrain - sum(r(t)' * ...
                                                          log(y(t, :)))) / n;
        errorRateOldTrain = errorRateTrain(iteration);
        
        % run through validation set
        % loop through rows of training data in random order
        validationDataRandom = validationData(randperm( ...
            size(trainingData, 1)), :);
        x = [repmat(1, 1, size(validationData, 1))' ... 
             validationData(:, 1:end - 1)];
        r = zeros(n, 1);
        r = validationDataRandom(:, end);
        for t = 1:n
            for h = 1:m
                o = dot(w(h, :), x(t, :));
                % ReLU activation
                if o < 0 
                    z(t, h) = 0;
                else 
                    z(t, h) = o;
                end
            end
            for ii = 1:k
                o = z(t, :) * v(ii, :)';
                % RELU activation
                if o < 0
                    y(t, ii) = 0;
                else
                    y(t, ii) = o;
                end
            end
        end
        % change in error rate for validation
        errorRateValid(iteration) = (errorRateOldValid - sum(r(t)' * ...
                                                          log(y(t, :)))) / n;
        errorRateOldValid = errorRateValid(iteration);
    end
    % print training and validation error rates
    errorRateTrain(end)
    errorRateValid(end)
end