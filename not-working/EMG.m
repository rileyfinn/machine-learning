function [h, m, Q] = emg(flag, imageFilePath, k)
    if flag == 0
        % standard EM implementation
        numOfIterations = 110;
        Q = zeros(numOfIterations, 2);
        [img cmap] = imread(imageFilePath);
        img_rgb = ind2rgb(img, cmap);
        img_double = im2double(img_rgb);
        numOfRows = size(img_double, 1) * ...
            size(img_double, 2);
        numOfCols = size(img_double, 3);
        imgMat = reshape(img_double, [numOfRows, numOfCols]);
        [indC, C] = kmeans(imgMat, k, 'EmptyAction', 'singleton');

        % initialize the means, cova matrices, and priors
        d = 3;
        m = zeros(k, d);
        Sigma = cell(k, 1);
        prior = zeros(k, 1);
        for ii = 1:k
            m(ii, :) = C(ii, :);
            Sigma{ii} = cov(imgMat(indC == ii, :));
            prior(ii) = sum(indC == ii) / length(indC);
        end
        if prior == 0
            prior = 0.0000000001;
        end

        for iteration = 1:numOfIterations
            % E-step: evaluate responsibilities of current parameter values
            N = length(imgMat);
            h = zeros(N, k);
            x = imgMat;
            numer = zeros(N, k);
            denom = zeros(1, k);

            for t = 1:N
                        for ii = 1:k
                    pdf =  mvnpdf(x(t, :), m(ii, :), Sigma{ii});
                    if pdf == 0
                        pdf = 0.0000000001;
                    end
                    numer(t, ii) = prior(ii) * pdf;
                        end
                        denom = sum(numer(t, :));
                        h(t, :) = numer(t, :) ./ denom;
            end

            % Evaluate the log likelihood for the E-step
            innerSum = zeros(N, k);
            %pdf = zeros(N, k);
            for t = 1:N
                for ii = 1:k
                    pdf = mvnpdf(x(t, :), m(ii, :), Sigma{ii});
                    % gets rid of log(0)'s
                    if pdf == 0
                        pdf = 0.00000001;
                    end
                    if prior == 0
                        prior = 0.00000001;
                    end
                    innerSum(t, ii) = h(t, ii) * ...
                        (log(prior(ii)) + log(pdf));
                end
            end
            Q(iteration, 1) = sum(sum(innerSum));

            % M-step: Re-estimate parameters using the current responsibilities
            % compute the new means
            % find which rows of x belong to which clusters
            maxCluster = zeros(N, 1);
            for t = 1:N
                [value, maxCluster(t)] = max(h(t, :));
            end

            mNew = zeros(k, d);
            for ii = 1:k
                newN = sum(maxCluster == ii);
                summationM = zeros(1, d);
                for t = 1:newN
                    summationM = summationM + (h(t, ii) * x(t, :));
                    % summationM = sum((h(t, ii) * x(t, :)));
                end
                mNew(ii, :) = summationM / newN;
                if newN == 0
                    mNew(ii, :) = 0;
                end
            end

            % compute the new Sigmas
            SigmaNew = cell(k, 1);
            newN = zeros(k, 1);
            summationSigma = cell(N, 1);
            for ii = 1:k
                newN(ii) = sum(maxCluster == ii);
                for t = 1:newN
                    if newN == 0
                        summationSigma{t} = zeros(d);
                    else
                        summationSigma{t} = (h(t, ii) * ...
                                             (x(t, :) - mNew(ii, :))' * ...
                                             (x(t, :) - mNew(ii, :)))/...
                            newN(ii);
                    end
                    if isnan(summationSigma{t})
                        summationSigma{t} = zeros(d);
                    end
                end
                SigmaNew{ii} = sum(cat(3, summationSigma{:}), 3);
            end

            % for ii = 1:k
            %     if any(eig(SigmaNew{ii}) < 0)
            %         SigmaNew{ii} = (SigmaNew{ii} + ...
            %                         SigmaNew{ii}.') / 2;
            %     end
            % end

            % compute the new mixing coefficients
            priorNew = zeros(k, 1);
            for ii = 1:k
                  newN = sum(maxCluster == ii);
                  priorNew(ii) = newN / length(maxCluster);
                  if priorNew(ii) == 0
                      priorNew(ii) = 0.000000001;
                  end
            end

            % Compute the log-likelihood for M-step
            innerSum = zeros(N, k);
            pdfNew = zeros(N, k);
            for t = 1:N
                for ii = 1:k
                    pdfNew = mvnpdf(x(t, :), mNew(ii, :), SigmaNew{ii});
                    % gets rid of log(0)'s
                    if pdfNew == 0
                        pdfNew = 0.000000001;
                    end
                    innerSum(t, ii) = h(t, ii) *  ...
                        (log(priorNew(ii)) + log(pdfNew));
                end
            end
            Q(iteration, 2) = sum(sum(innerSum));

            if abs(Q(iteration, 2) - Q(iteration, 1)) >  0.01
                prior = priorNew;
                m = mNew;
                Sigma = SigmaNew;
                continue
            else
                break
            end
        end

        % plot expected complete log-likelihood values
        clf
        figure;
        plotE = Q(:, 1)';
        plotM = Q(:, 2)';
        xAxis = 1:iteration;
        plot(xAxis, plotE ~= 0);
        hold on
        plot(xAxis, plotM ~= 0);
        legend('After M-step', 'After E-step');
        xlabel('Iteration');
        ylabel('Log-Likelihood');
        formatSpec = 'Expected Complete Log-Likelihood for k = %d';
        title(sprintf(formatSpec, k));

        % display single compressed image
        for t = 1:N
            [value, maxH] = max(h(t, :));
        end
        imgMatNew = reshape(img_double, numOfRows, numOfCols, k);
        figure
        imagesc(imgMatNew);

    else
        % improved EM algorithm
    end
end
