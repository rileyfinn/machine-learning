% mlptest(
% test_data.txt: path to test data file, 
% w: m×(d+1) matrix of input unit weights, 
% v: k×(m+1) matrix of hidden unit weights)
% function must return in variables the following outputs
% z: n×m matrix of hidden unit values where n is the number of
% training samples
% function must also print the test set error rate for the given function parameters


function z = mlptest(testDataFilePath, w, v)
% run through test set
    testData = testDataFilePath;
    testDataRandom = testData(randperm( ...
        size(testData, 1)), :);
    x = [repmat(1, 1, size(testData, 1))' ... 
         testData(:, 1:end - 1)];
    n = size(testData, 1);
    k = 10;
    m = 15;
    r = zeros(n, 1);
    r = testDataRandom(:, end);
    d = size(testData(:, 1:end - 1), 2);
    n = size(testData, 1);
    y = zeros(n, k);
    z = zeros(n, m);
    z = [repmat(1, 1, n)' z]; % add bias vector    
    for t = 1:n
        for h = 1:m
            o = dot(w(h, :), x(t, :));
            % ReLU activation
            if o < 0 
            z(t, h) = 0;
            else 
                z(t, h) = o;
            end
        end
        for ii = 1:k
            % ii = 1
            o = z(t, :) * v(ii, :)';
            % RELU activation
            if o < 0
                y(t, ii) = 0;
            else
                y(t, ii) = o;
            end
        end
        % errorRateTest(iteration) = (errorRateOldTest - sum(r(t)' * ...
        %                                                   log(y(t, :)))) / n;
        % errorRateOldTest = errorRateTest(iteration);
        errorRateTest(t, :) = (r(t)' * log(y(t, :)));
    end
    errorRateTestTotal = -sum(sum(errorRateTest))
end

